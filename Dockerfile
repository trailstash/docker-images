# Build tilemaker
FROM ubuntu:jammy AS builder
RUN env DEBIAN_FRONTEND=noninteractive apt update && env DEBIAN_FRONTEND=noninteractive apt install -y python3 build-essential libboost-dev libboost-filesystem-dev libboost-iostreams-dev libboost-program-options-dev libboost-system-dev liblua5.1-0-dev libprotobuf-dev libshp-dev libsqlite3-dev protobuf-compiler rapidjson-dev curl && rm -rf /var/lib/apt/lists/*
RUN curl -LO https://github.com/systemed/tilemaker/archive/refs/tags/v2.4.0.tar.gz
RUN tar xf v2.4.0.tar.gz
RUN make -C tilemaker-2.4.0 "CONFIG=-DFLOAT_Z_ORDER"
RUN make -C tilemaker-2.4.0 install

# bild runtime image
FROM ubuntu:jammy
# install osmiumm, osmosis, curl, gdal, & jq
RUN env DEBIAN_FRONTEND=noninteractive apt update && env DEBIAN_FRONTEND=noninteractive apt install -y gdal-bin osmium-tool osmosis curl jq && rm -rf /var/lib/apt/lists/*
# install pmtiles cli
RUN curl -L https://github.com/protomaps/go-pmtiles/releases/download/v1.7.0/go-pmtiles_1.7.0_Linux_x86_64.tar.gz | tar -z -C /usr/bin -x pmtiles
# install spreet
RUN curl -L https://github.com/flother/spreet/releases/download/v0.7.0/spreet-x86_64-unknown-linux-musl.tar.gz | tar -z -C /usr/bin -x spreet
# install tilemaker & deps
RUN env DEBIAN_FRONTEND=noninteractive apt update && env DEBIAN_FRONTEND=noninteractive apt install -y libboost-filesystem1.74.0 libboost-iostreams1.74.0 libboost-program-options1.74.0 libboost-system1.74.0 liblua5.1-0 libshp2 libsqlite3-0 libprotobuf23 && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/bin/tilemaker /usr/bin/tilemaker
# install pip & requests
RUN env DEBIAN_FRONTEND=noninteractive apt update && env DEBIAN_FRONTEND=noninteractive apt install -y make python3-pip && rm -rf /var/lib/apt/lists/*
RUN pip install requests
# install node
RUN curl https://nodejs.org/dist/v18.16.0/node-v18.16.0-linux-x64.tar.gz | tar -z -C /opt -x
ENV PATH="${PATH}:/opt/node-v18.16.0-linux-x64/bin"
