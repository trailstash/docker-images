# CI Builder Images
A Docker images for TrailStash

## Images
### `registry.gitlab.com/trailstash/docker-images/trailtash-gis:latest`

Also available as `registry.gitlab.com/trailstash/docker-images/osmium-tilemaker-pmtiles:latest` for legacy reasons
#### Includes:
* GDAL (`ogr2ogr` etc)
* `osmium`
* `tilemaker`
* `pmtiles`
* `curl`
* `python3`
* `pip3`
* `make`
* `jq`
* `spreet`
* `node`
* `osmosis`
